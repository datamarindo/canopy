---
title: "Análisis de dosel con modelos de elevación de dron: prospectiva para restauración ecológica en dos ecosistemas"
author: "Elio Díaz"
---


# Fundamentos

Los modelos digitales de elevación creados a partir de imágenes de drones pueden servir como insumo principal para hacer análisis de dosel de paisajes, con la ventaja de ser mucho más económicos que las imágenes Lidar, una ventaja aplicable sobre todo en _modelos de superficie_; sin embargo, frente a esta otra tecnología, las imágenes de drone tienen la limitante de no poder atravesar las hojas de los árboles y por ende dar pobres resultados en _los modelos de terreno_.

Usando ambos modelos se puede obtener la altura mediante restar el modelo de terreno del modelo de la superficie, a este resultado se le conoce como _modelo de altura de dosel_ o _CHM_ (canopy height model) el cual se usa como insumo para el análisis. Esto es válido sobre todo en casos con _paisajes perforados o percolados_: aquellos que tienen suelo desnudo entremezclado con árboles.


Dos algoritmos son los que se utilizan para el análisis de dosel, del paquete `ForestTools` [@foresttools] del lenguaje de computación estadística __R__ :

1. El _filtro de ventana variable_ o _vwf_, que sirve para detectar las cúspides, buscando los puntos más altos dentro de un radio alrededor de cada celda; el radio puede variar en función, por ejemplo, de la altura del _CHM_.
1. Un algoritmo de cuencas, modificado de los que se usan en hidrología, para dividir correspondientemente los doseles de árboles que yacen juntos.

# Pruebas en actividades de Pronatura Veracruz

  Se hicieron dos análisis de copas para determinar las posibilidades de usar los modelos digitales obtenidos con dron, en dos ambientes distintos:

1. Finca cafetalera de sombra, con muchas pendientes en el terreno, árboles de sombra de alturas muy variables, de hasta 30 m.
1. Restauración de manglar de 5 años, realizada en tulillo _Eleocharis sp_, con presencia de mangles maduros y altos. 


# Resultados
## Finca cafetalera en Teocelo

En la finca cafetalera se usaron distintas alturas mínimas del CHM para el algoritmo de _vwf_, encontrándose mejores resultados a 2 m; el algoritmo detectó 159 cúspides y un área de dosel de 3,676 m², de 6,184 m² totales en la imagen. 

<img src="teocelo_barriales.png"  width="700" height="250">

En la imagen de esta finca bajo sombra se observa una muy buena división de las copas de distintos árboles; en algunos casos se una copa es dividida en dos, probablemente porque tenga dos puntos de altura, como sucede en árboles que no tienen copas semiesféricas. En una zona donde hay un cafetal recientemente plantado no se reconocen árboles, solamente unos cafetos ya maduros y un plátano, esto es consecuencia de la altura de vuelo, que en este caso fue de 80 m, y la resultante resolución altitudinal, que hace que los dos modelos no tengan diferencias de altura en esa zona.

<img src="teocelo_arbolesRGB.png"  width="300" height="300">


## Restauración de Manglar, predio El Pájaro

En la restauración de manglar se detectaron 81 cúspides, con una superficie de dosel de 1,137 m², de un total de 2,225 m². 

<img src="pajaro.png"  width="600" height="250">

En el paisaje se reconocen virtualmente todas las copas de esta restauración; actualmente muchos doseles de los núcleos ya están en contacto. Es importante mencionar que el algoritmo puede encontrar los elementos del paisaje a pesar de que la imagen es borrosa, debido a la condensación en el lente del dron.

<img src="pajaroRGB.png"  width="300" height="300">


# Conclusiones
El análisis digitalizado de dosel a partir de modelos de elevación de dron ofrece un complemento al monitoreo de desarrollo de restauraciones, más rápido y menos demandante de labor que la digitalización manual, incluso previa segmentación de imágenes.
